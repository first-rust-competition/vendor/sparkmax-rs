//! FFI bindings for the SPARK MAX heartbeat driver.

use std::os::raw::c_int;

extern "C" {
    #[link_name = "REV_CANSparkMaxHeartbeatInit"]
    pub fn init();
    #[link_name = "REV_CANSparkMaxRunHeartbeat"]
    pub fn run() -> i32;
    #[link_name = "REV_CANSparkMaxRegisterDevice"]
    pub fn register_device(deviceID: c_int);
    #[link_name = "REV_CANSparkMaxUnregisterDevice"]
    pub fn unregister_device(deviceID: c_int);
}
