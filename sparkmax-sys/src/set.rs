//! FFI bindings for the SPARK MAX set driver.

use std::os::raw::c_int;

extern "C" {
    #[link_name = "REV_CANSparkMaxSetDriverInit"]
    pub fn init();
    #[link_name = "REV_CANSparkMaxRunSetDriver"]
    pub fn run() -> i32;
    #[link_name = "REV_CANSparkMaxSetDriverSet4"]
    pub fn set4(deviceID: u32, value: f32, apiId: i32, pidSlot: u8, arbFF: i16);
    #[link_name = "REV_CANSparkMaxSetDriverSet5"]
    pub fn set5(deviceID: u32, value: f32, apiId: i32, pidSlot: u8, arbFF: i16, arbFFUnits: u8);
    #[link_name = "REV_CANSparkMaxSetDriverSetCtrlFramePeriodMs"]
    pub fn set_ctrl_frame_period_ms(deviceID: u32, periodMs: c_int);
}
