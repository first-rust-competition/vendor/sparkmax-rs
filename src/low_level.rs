//! Provides access to low-level SPARK MAX settings and other properties.
//! It is unlikely you will want to access this module directly.

use std::{error, fmt};
use wpilib_sys::HAL_CANManufacturer;

use super::{frames, Result};
use sparkmax_sys as driver;

lazy_static::lazy_static! {
    static ref HEARTBEAT_INIT: i32 = {
        unsafe {
            driver::heartbeat::init();
            driver::heartbeat::run()
        }
    };
    static ref SET_DRIVER_INIT: i32 = {
        unsafe {
            driver::set::init();
            driver::set::run()
        }
    };
}

/// Initialize the SPARK MAX driver library.
/// Does nothing if already initialized.
pub fn ensure_init() {
    lazy_static::initialize(&HEARTBEAT_INIT);
    lazy_static::initialize(&SET_DRIVER_INIT);
}

#[repr(u8)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum MotorType {
    Brushed = 0,
    Brushless = 1,
}
impl Default for MotorType {
    #[inline]
    fn default() -> Self {
        MotorType::Brushless
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum ParameterError {
    InvalidID = 1,
    MismatchType = 2,
    AccessMode = 3,
    Invalid = 4,
    NotImplementedDeprecated = 5,
}
impl fmt::Display for ParameterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::ParameterError::*;
        f.write_str(match self {
            InvalidID => "Invalid parameter ID",
            MismatchType => "Mismatching parameter type specified",
            AccessMode => "Invalid access mode for parameter",
            Invalid => "Parameter invalid (value?)",
            NotImplementedDeprecated => "Not implemented and/or deprecated parameter",
        })
    }
}
impl error::Error for ParameterError {}

macro_rules! parameters {
    ($($name:ident($type:ident) = $id:expr,)*) => {
        #[allow(non_camel_case_types)]
        #[repr(i32)]
        #[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
        pub enum ConfigParameter {
            $($name = $id,)*
        }

        impl ConfigParameter {
            pub fn type_(self) -> ParameterType {
                match self {
                    $(ConfigParameter::$name => ParameterType::$type,)*
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Copy, Clone, PartialEq)]
        pub enum Parameter {
            $($name($type),)*
        }

        impl Parameter {
            pub fn id(&self) -> ConfigParameter {
                match self {
                    $(Parameter::$name(_) => ConfigParameter::$name,)*
                }
            }

            pub(crate) fn value(self) -> u32 {
                match self {
                    $(Parameter::$name(v) => v.to_bits(),)*
                }
            }
        }
    }
}

trait ToBits {
    fn to_bits(self) -> u32;
}
impl ToBits for i32 {
    #[inline]
    fn to_bits(self) -> u32 {
        self as u32
    }
}
impl ToBits for u32 {
    #[inline]
    fn to_bits(self) -> u32 {
        self
    }
}
impl ToBits for bool {
    #[inline]
    fn to_bits(self) -> u32 {
        self as u32
    }
}

parameters! {
    CanID(u32) = 0,
    InputMode(u32) = 1,
    MotorType(u32) = 2,
    CommAdvance(f32) = 3,
    SensorType(u32) = 4,
    CtrlType(u32) = 5,
    IdleMode(u32) = 6,
    InputDeadband(f32) = 7,
    FirmwareVer(u32) = 8,
    HallOffset(i32) = 9,
    PolePairs(u32) = 10,
    CurrentChop(f32) = 11,
    CurrentChopCycles(u32) = 12,
    P_0(f32) = 13,
    I_0(f32) = 14,
    D_0(f32) = 15,
    F_0(f32) = 16,
    IZone_0(f32) = 17,
    DFilter_0(f32) = 18,
    OutputMin_0(f32) = 19,
    OutputMax_0(f32) = 20,
    P_1(f32) = 21,
    I_1(f32) = 22,
    D_1(f32) = 23,
    F_1(f32) = 24,
    IZone_1(f32) = 25,
    DFilter_1(f32) = 26,
    OutputMin_1(f32) = 27,
    OutputMax_1(f32) = 28,
    P_2(f32) = 29,
    I_2(f32) = 30,
    D_2(f32) = 31,
    F_2(f32) = 32,
    IZone_2(f32) = 33,
    DFilter_2(f32) = 34,
    OutputMin_2(f32) = 35,
    OutputMax_2(f32) = 36,
    P_3(f32) = 37,
    I_3(f32) = 38,
    D_3(f32) = 39,
    F_3(f32) = 40,
    IZone_3(f32) = 41,
    DFilter_3(f32) = 42,
    OutputMin_3(f32) = 43,
    OutputMax_3(f32) = 44,
    Reserved(u32) = 45,
    OutputRatio(f32) = 46,
    SerialNumberLow(u32) = 47,
    SerialNumberMid(u32) = 48,
    SerialNumberHigh(u32) = 49,
    LimitSwitchFwdPolarity(bool) = 50,
    LimitSwitchRevPolarity(bool) = 51,
    HardLimitFwdEn(bool) = 52,
    HardLimitRevEn(bool) = 53,
    SoftLimitFwdEn(bool) = 54,
    SoftLimitRevEn(bool) = 55,
    OpenLoopRampRate(f32) = 56,
    FollowerID(u32) = 57,
    FollowerConfig(u32) = 58,
    SmartCurrentStallLimit(u32) = 59,
    SmartCurrentFreeLimit(u32) = 60,
    SmartCurrentConfig(u32) = 61,
    SmartCurrentReserved(u32) = 62,
    MotorKv(u32) = 63,
    MotorR(u32) = 64,
    MotorL(u32) = 65,
    MotorRsvd1(u32) = 66,
    MotorRsvd2(u32) = 67,
    MotorRsvd3(u32) = 68,
    EncoderCountsPerRev(u32) = 69,
    EncoderAverageDepth(u32) = 70,
    EncoderSampleDelta(u32) = 71,
    EncoderRsvd0(u32) = 72,
    EncoderRsvd1(u32) = 73,
    VoltageCompMode(u32) = 74,
    CompensatedNominalVoltage(f32) = 75,
    SmartMotionMaxVelocity_0(f32) = 76,
    SmartMotionMaxAccel_0(f32) = 77,
    SmartMotionMinVelOutput_0(f32) = 78,
    SmartMotionAllowedClosedLoopError_0(f32) = 79,
    SmartMotionAccelStrategy_0(f32) = 80,
    SmartMotionMaxVelocity_1(f32) = 81,
    SmartMotionMaxAccel_1(f32) = 82,
    SmartMotionMinVelOutput_1(f32) = 83,
    SmartMotionAllowedClosedLoopError_1(f32) = 84,
    SmartMotionAccelStrategy_1(f32) = 85,
    SmartMotionMaxVelocity_2(f32) = 86,
    SmartMotionMaxAccel_2(f32) = 87,
    SmartMotionMinVelOutput_2(f32) = 88,
    SmartMotionAllowedClosedLoopError_2(f32) = 89,
    SmartMotionAccelStrategy_2(f32) = 90,
    SmartMotionMaxVelocity_3(f32) = 91,
    SmartMotionMaxAccel_3(f32) = 92,
    SmartMotionMinVelOutput_3(f32) = 93,
    SmartMotionAllowedClosedLoopError_3(f32) = 94,
    SmartMotionAccelStrategy_3(f32) = 95,
    IMaxAccum_0(f32) = 96,
    Slot3Placeholder1_0(f32) = 97,
    Slot3Placeholder2_0(f32) = 98,
    Slot3Placeholder3_0(f32) = 99,
    IMaxAccum_1(f32) = 100,
    Slot3Placeholder1_1(f32) = 101,
    Slot3Placeholder2_1(f32) = 102,
    Slot3Placeholder3_1(f32) = 103,
    IMaxAccum_2(f32) = 104,
    Slot3Placeholder1_2(f32) = 105,
    Slot3Placeholder2_2(f32) = 106,
    Slot3Placeholder3_2(f32) = 107,
    IMaxAccum_3(f32) = 108,
    Slot3Placeholder1_3(f32) = 109,
    Slot3Placeholder2_3(f32) = 110,
    Slot3Placeholder3_3(f32) = 111,
    PositionConversionFactor(f32) = 112,
    VelocityConversionFactor(f32) = 113,
    ClosedLoopRampRate(f32) = 114,
    SoftLimitFwd(f32) = 115,
    SoftLimitRev(f32) = 116,
}

#[repr(u8)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum ParameterType {
    Int32 = 0,
    Uint32 = 1,
    Float32 = 2,
    Bool = 3,
}
#[allow(non_upper_case_globals)]
impl ParameterType {
    const i32: ParameterType = ParameterType::Int32;
    const u32: ParameterType = ParameterType::Uint32;
    const f32: ParameterType = ParameterType::Float32;
    const bool: ParameterType = ParameterType::Bool;
}

#[derive(Debug, Clone)]
pub struct PeriodicStatus0 {
    pub applied_output: f64,
    pub faults: u16,
    pub sticky_faults: u16,
    pub idle_mode: u8,
    // pub motor_type: MotorType,
    pub is_follower: bool,
}

#[derive(Debug, Clone)]
pub struct PeriodicStatus1 {
    pub sensor_velocity: f32,
    pub motor_temperature: u8,
    pub bus_voltage: f64,
    pub output_current: f64,
}

#[derive(Debug, Clone)]
pub struct PeriodicStatus2 {
    pub sensor_position: f32,
    pub iaccum: f32,
}

/// Periodic status frames.
///
/// This trait is sealed and cannot be implemented outside of this crate.
pub trait PeriodicFrame: private::Sealed {
    const FRAME_ID: i32;
    const DEFAULT_PERIOD_MS: u16;
}
impl PeriodicFrame for PeriodicStatus0 {
    const FRAME_ID: i32 = frames::CMD_API_STAT0;
    const DEFAULT_PERIOD_MS: u16 = 10;
}
impl PeriodicFrame for PeriodicStatus1 {
    const FRAME_ID: i32 = frames::CMD_API_STAT1;
    const DEFAULT_PERIOD_MS: u16 = 20;
}
impl PeriodicFrame for PeriodicStatus2 {
    const FRAME_ID: i32 = frames::CMD_API_STAT2;
    const DEFAULT_PERIOD_MS: u16 = 50;
}

pub const DEFAULT_CAN_TIMEOUT_MS: i32 = 20;

// FIXME: remove when added to roboRIO image
#[allow(non_upper_case_globals)]
pub(crate) const HAL_CAN_Man_kREV: HAL_CANManufacturer::Type = 5;

/// Contains low-level calls to a SPARK MAX over CAN.
/// It is unlikely you will need to use these directly.
///
/// This trait is sealed and cannot be implemented outside of this crate.
pub trait SparkMaxLowLevel: private::Sealed {
    fn firmware_version(&self) -> (u32, bool);

    fn set_motor_type(&mut self, _: MotorType) -> Result<()>;
    fn motor_type(&self) -> Result<MotorType>;

    fn set_periodic_frame_period<F: PeriodicFrame>(&mut self, period_ms: u16) -> Result<()>;
    fn set_control_frame_period(&mut self, period_ms: i32);

    fn set_parameter(&mut self, parameter: Parameter) -> Result<()>;

    fn parameter_bool(&self, parameter_id: ConfigParameter) -> Result<bool>;
    fn parameter_float(&self, parameter_id: ConfigParameter) -> Result<f32>;
    fn parameter_u32(&self, parameter_id: ConfigParameter) -> Result<u32>;
    fn parameter_i32(&self, parameter_id: ConfigParameter) -> Result<i32>;

    fn set_enc_position(&mut self, value: f32) -> Result<()>;
    fn set_iaccum(&mut self, value: f32) -> Result<()>;

    fn restore_factory_defaults(&mut self, persist: bool) -> Result<()>;
}

mod private {
    use crate::SparkMax;

    pub trait Sealed {}
    impl Sealed for SparkMax {}
    impl Sealed for super::PeriodicStatus0 {}
    impl Sealed for super::PeriodicStatus1 {}
    impl Sealed for super::PeriodicStatus2 {}
}
