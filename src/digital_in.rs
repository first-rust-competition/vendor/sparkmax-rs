use super::{
    low_level::{ConfigParameter, SparkMaxLowLevel},
    Faults, Result, SparkMax,
};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum LimitSwitch {
    Forward,
    Reverse,
}

#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum LimitSwitchPolarity {
    NormallyOpen = 0,
    NormallyClosed = 1,
}

#[derive(Debug)]
pub struct DigitalInput<'a> {
    device: &'a SparkMax,
    limit_switch: LimitSwitch,
}

impl<'a> DigitalInput<'a> {
    pub const fn new(device: &'a SparkMax, limit_switch: LimitSwitch) -> DigitalInput<'a> {
        DigitalInput {
            device,
            limit_switch,
        }
    }

    pub fn get(&self) -> Result<bool> {
        Ok(self.device.faults()?.contains(match self.limit_switch {
            LimitSwitch::Forward => Faults::HARD_LIMIT_FWD,
            LimitSwitch::Reverse => Faults::HARD_LIMIT_REV,
        }))
    }

    pub fn is_limit_switch_enabled(&self) -> Result<bool> {
        self.device.parameter_bool(match self.limit_switch {
            LimitSwitch::Forward => ConfigParameter::HardLimitFwdEn,
            LimitSwitch::Reverse => ConfigParameter::HardLimitRevEn,
        })
    }
}

#[derive(Debug)]
pub struct DigitalInputMut<'a> {
    device: &'a mut SparkMax,
    limit_switch: LimitSwitch,
}

impl DigitalInputMut<'_> {
    pub fn new(
        device: &mut SparkMax,
        limit_switch: LimitSwitch,
        polarity: LimitSwitchPolarity,
    ) -> DigitalInputMut<'_> {
        let _ = device.set_parameter_bool(
            match limit_switch {
                LimitSwitch::Forward => ConfigParameter::LimitSwitchFwdPolarity,
                LimitSwitch::Reverse => ConfigParameter::LimitSwitchRevPolarity,
            },
            polarity as u32 != 0,
        );
        DigitalInputMut {
            device,
            limit_switch,
        }
    }

    pub fn get(&self) -> Result<bool> {
        Ok(self.device.faults()?.contains(match self.limit_switch {
            LimitSwitch::Forward => Faults::HARD_LIMIT_FWD,
            LimitSwitch::Reverse => Faults::HARD_LIMIT_REV,
        }))
    }

    pub fn enable_limit_switch(&mut self, enable: bool) -> Result<()> {
        self.device.set_parameter_bool(
            match self.limit_switch {
                LimitSwitch::Forward => ConfigParameter::HardLimitFwdEn,
                LimitSwitch::Reverse => ConfigParameter::HardLimitRevEn,
            },
            enable,
        )
    }

    pub fn is_limit_switch_enabled(&self) -> Result<bool> {
        self.device.parameter_bool(match self.limit_switch {
            LimitSwitch::Forward => ConfigParameter::HardLimitFwdEn,
            LimitSwitch::Reverse => ConfigParameter::HardLimitRevEn,
        })
    }
}

impl<'a, 'b: 'a> From<DigitalInputMut<'b>> for DigitalInput<'a> {
    fn from(input: DigitalInputMut<'b>) -> Self {
        DigitalInput {
            device: input.device,
            limit_switch: input.limit_switch,
        }
    }
}
