use bitflags::bitflags;
use sparkmax_sys as driver;
use std::mem;
use wpilib::Can;
use wpilib_sys::{usage, HAL_CANDeviceType::HAL_CAN_Dev_kMotorController};

use super::low_level::HAL_CAN_Man_kREV;
use super::{
    digital_in::*,
    frames,
    low_level::{self, ConfigParameter, Parameter, PeriodicFrame, SparkMaxLowLevel},
    ControlType, Encoder, Error, MotorType, Result,
};

#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum IdleMode {
    Coast = 0,
    Brake = 1,
}
impl Default for IdleMode {
    #[inline]
    fn default() -> Self {
        IdleMode::Coast
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum SoftLimitDirection {
    Forward,
    Reverse,
}

bitflags! {
    pub struct Faults: u16 {
        const BROWNOUT = 1 << 0;
        const OVERCURRENT = 1 << 1;
        const IWDT_RESET = 1 << 2;
        const MOTOR_FAULT = 1 << 3;
        const SENSOR_FAULT = 1 << 4;
        const STALL = 1 << 5;
        const EEPROMCRC = 1 << 6;
        const CANTX = 1 << 7;
        const CANRX = 1 << 8;
        const HAS_RESET = 1 << 9;
        const DRV_FAULT = 1 << 10;
        const OTHER_FAULT = 1 << 11;
        const SOFT_LIMIT_FWD = 1 << 12;
        const SOFT_LIMIT_REV = 1 << 13;
        const HARD_LIMIT_FWD = 1 << 14;
        const HARD_LIMIT_REV = 1 << 15;
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Leader {
    Disabled,
    SparkMax(u8),
    Phoenix(u32),
}
impl Leader {
    pub fn arb_id(&self) -> u32 {
        use self::Leader::*;
        match self {
            Disabled => 0,
            SparkMax(id) => 0x0205_1800 | *id as u32,
            Phoenix(id) => 0x0204_0080 | id,
        }
    }
    pub fn config_id(&self) -> u8 {
        use self::Leader::*;
        match self {
            Disabled => 0,
            SparkMax(_) => 26,
            Phoenix(_) => 27,
        }
    }
}
impl From<&SparkMax> for Leader {
    fn from(spark: &SparkMax) -> Leader {
        Leader::SparkMax(spark.device_id)
    }
}
impl Default for Leader {
    /// Returns `Leader::Disabled`.
    fn default() -> Self {
        Leader::Disabled
    }
}

#[repr(C, align(4))]
#[derive(Debug, Copy, Clone)]
struct FollowerConfig(frames::__BindgenBitfieldUnit<[u8; 4], u32>);

impl FollowerConfig {
    fn new(invert: bool, predefined: u8) -> FollowerConfig {
        let mut __bindgen_bitfield_unit: frames::__BindgenBitfieldUnit<[u8; 4usize], u32> =
            Default::default();
        __bindgen_bitfield_unit.set_bit(18usize, invert);
        __bindgen_bitfield_unit.set(24usize, 8u8, predefined as u64);
        FollowerConfig(__bindgen_bitfield_unit)
    }

    fn to_bits(self) -> u32 {
        unsafe { mem::transmute(self) }
    }
}

#[derive(Debug)]
pub struct SparkMax {
    can: Can,
    can_timeout_ms: i32,
    inverted: bool,
    device_id: u8,
    status_period_ms: [u16; 3],
}

impl SparkMax {
    pub fn new(device_id: u8, type_: MotorType) -> Result<SparkMax> {
        low_level::ensure_init();
        let can = Can::new(
            device_id as _,
            HAL_CAN_Man_kREV,
            HAL_CAN_Dev_kMotorController,
        )?;
        let mut spark = SparkMax {
            can,
            can_timeout_ms: low_level::DEFAULT_CAN_TIMEOUT_MS,
            inverted: false,
            device_id,
            status_period_ms: [
                low_level::PeriodicStatus0::DEFAULT_PERIOD_MS,
                low_level::PeriodicStatus1::DEFAULT_PERIOD_MS,
                low_level::PeriodicStatus2::DEFAULT_PERIOD_MS,
            ],
        };

        // TODO: rest of low-level constructor (firmware version check)

        spark.set_motor_type(type_)?;

        unsafe {
            driver::heartbeat::register_device(device_id as _);
            // Set default control frame period
            driver::set::set_ctrl_frame_period_ms(device_id as _, 10);
        }

        usage::report(usage::resource_types::RevSparkMaxCAN, device_id as _);

        // Initialize conversion factors to 1
        spark.set_position_conversion_factor(1.0)?;
        spark.set_velocity_conversion_factor(1.0)?;
        let _ = spark.clear_faults();
        Ok(spark)
    }

    pub const fn device_id(&self) -> u8 {
        self.device_id
    }

    pub fn set(&mut self, speed: f32) {
        self.setpoint_command(speed, ControlType::DutyCycle, 0, 0.0, 0)
    }

    pub fn set_inverted(&mut self, is_inverted: bool) {
        self.inverted = is_inverted;
    }

    pub fn inverted(&self) -> bool {
        self.inverted
    }

    pub fn forward_limit_switch(&self) -> DigitalInput<'_> {
        DigitalInput::new(self, LimitSwitch::Forward)
    }

    pub fn reverse_limit_switch(&self) -> DigitalInput<'_> {
        DigitalInput::new(self, LimitSwitch::Reverse)
    }

    pub fn forward_limit_switch_mut(
        &mut self,
        polarity: LimitSwitchPolarity,
    ) -> DigitalInputMut<'_> {
        DigitalInputMut::new(self, LimitSwitch::Forward, polarity)
    }

    pub fn reverse_limit_switch_mut(
        &mut self,
        polarity: LimitSwitchPolarity,
    ) -> DigitalInputMut<'_> {
        DigitalInputMut::new(self, LimitSwitch::Reverse, polarity)
    }

    pub fn set_secondary_current_limit(&mut self, limit: f32, chop_cycles: u32) -> Result<()> {
        self.set_parameter(Parameter::CurrentChop(limit))?;
        self.set_parameter(Parameter::CurrentChopCycles(chop_cycles))
    }

    pub fn set_idle_mode(&mut self, idle_mode: IdleMode) -> Result<()> {
        self.set_parameter(Parameter::IdleMode(idle_mode as u32))
    }

    pub fn enable_voltage_compensation(&mut self, nominal_voltage: f32) -> Result<()> {
        self.set_parameter(Parameter::CompensatedNominalVoltage(nominal_voltage))?;
        self.set_parameter(Parameter::VoltageCompMode(2))
    }

    pub fn disable_voltage_compensation(&mut self) -> Result<()> {
        self.set_parameter(Parameter::VoltageCompMode(0))?;
        self.set_parameter(Parameter::CompensatedNominalVoltage(0.0))
    }

    pub fn set_open_loop_ramp_rate(&mut self, rate: f32) -> Result<()> {
        self.set_parameter_float(
            ConfigParameter::OpenLoopRampRate,
            if rate != 0.0 { 1.0 / rate } else { rate },
        )
    }

    pub fn set_closed_loop_ramp_rate(&mut self, rate: f32) -> Result<()> {
        self.set_parameter_float(
            ConfigParameter::ClosedLoopRampRate,
            if rate != 0.0 { 1.0 / rate } else { rate },
        )
    }

    pub fn follow(&mut self, leader: impl Into<Leader>, invert: bool) -> Result<()> {
        let leader = leader.into();
        let leader_arb_id = leader.arb_id();
        let predefined = leader.config_id();
        let config_raw = FollowerConfig::new(invert, predefined).to_bits();

        let frame = frames::frc_dataframe_t {
            follower_out: frames::frc_dataframe_follower_out_t {
                followerCfg: config_raw,
                followerID: leader_arb_id,
            },
        };
        let frame_data = unsafe { frame.data };
        self.can
            .write_packet(&frame_data, frames::CMD_API_SET_FOLLOWER)?;

        // Confirm good setup checking response
        match self
            .can
            .read_packet_timeout(frames::CMD_API_SET_FOLLOWER, self.can_timeout_ms)?
        {
            None => Err(Error::Timeout),
            Some(return_frame) => {
                if frame_data != return_frame.data() {
                    Err(Error::FollowConfigMismatch)
                } else {
                    Ok(())
                }
            }
        }
    }

    pub fn faults(&self) -> Result<Faults> {
        Ok(Faults::from_bits_truncate(self.periodic_status_0()?.faults))
    }

    pub fn sticky_faults(&self) -> Result<Faults> {
        Ok(Faults::from_bits_truncate(
            self.periodic_status_0()?.sticky_faults,
        ))
    }

    pub fn clear_faults(&mut self) -> Result<()> {
        self.can.write_packet(&[], frames::CMD_API_CLEAR_FAULTS)?;
        Ok(())
    }

    pub fn burn_flash(&mut self) -> Result<()> {
        self.can
            .write_packet(&[0xA3, 0x3A], frames::CMD_API_BURN_FLASH)?;
        Ok(())
    }

    pub fn set_can_timeout(&mut self, milliseconds: i32) {
        self.can_timeout_ms = milliseconds;
    }

    pub fn enable_soft_limit(&mut self, direction: SoftLimitDirection, enable: bool) -> Result<()> {
        self.set_parameter_bool(
            match direction {
                SoftLimitDirection::Forward => ConfigParameter::SoftLimitFwdEn,
                SoftLimitDirection::Reverse => ConfigParameter::SoftLimitRevEn,
            },
            enable,
        )
    }

    pub fn set_soft_limit(&mut self, direction: SoftLimitDirection, limit: f32) -> Result<()> {
        self.set_parameter_float(
            match direction {
                SoftLimitDirection::Forward => ConfigParameter::SoftLimitFwd,
                SoftLimitDirection::Reverse => ConfigParameter::SoftLimitRev,
            },
            limit,
        )
    }

    pub fn soft_limit(&self, direction: SoftLimitDirection) -> Result<f32> {
        self.parameter_float(match direction {
            SoftLimitDirection::Forward => ConfigParameter::SoftLimitFwd,
            SoftLimitDirection::Reverse => ConfigParameter::SoftLimitRev,
        })
    }

    pub fn is_soft_limit_enabled(&self, direction: SoftLimitDirection) -> Result<bool> {
        self.parameter_bool(match direction {
            SoftLimitDirection::Forward => ConfigParameter::SoftLimitFwdEn,
            SoftLimitDirection::Reverse => ConfigParameter::SoftLimitRevEn,
        })
    }
}

/// Low-level protected methods.
impl SparkMax {
    pub(crate) fn periodic_status_0(&self) -> Result<low_level::PeriodicStatus0> {
        let frame = self.can.read_periodic_packet(
            frames::CMD_API_STAT0,
            self.status_period_ms[0] as i32 * 2 + self.can_timeout_ms,
            self.status_period_ms[0] as i32,
        )?;
        match frame {
            None => Err(Error::Timeout),
            Some(frame) => {
                let frame_data = frame.data();
                if frame_data.len() < mem::size_of::<frames::frc_dataframe_status0_in_t>() {
                    return Err(Error::Error);
                }
                let status0_frame: &frames::frc_dataframe_status0_in_t =
                    unsafe { &*(frame_data.as_ptr() as *const frames::frc_dataframe_status0_in_t) };
                Ok(low_level::PeriodicStatus0 {
                    applied_output: (status0_frame.appliedOutput
                        * if self.inverted { -1 } else { 1 })
                        as f64
                        / 32767.0,
                    faults: status0_frame.faults,
                    idle_mode: status0_frame.idleMode(),
                    is_follower: status0_frame.is_follower() != 0,
                    // motor_type: unsafe { mem::transmute(status0_frame.mtrType()) },
                    sticky_faults: status0_frame.stickyFaults,
                })
            }
        }
    }

    pub(crate) fn periodic_status_1(&self) -> Result<low_level::PeriodicStatus1> {
        match self.can.read_periodic_packet(
            frames::CMD_API_STAT1,
            self.status_period_ms[1] as i32 * 2 + self.can_timeout_ms,
            self.status_period_ms[1] as i32,
        )? {
            None => Err(Error::Timeout),
            Some(frame) => {
                let frame_data = frame.data();
                if frame_data.len() < mem::size_of::<frames::frc_dataframe_status1_in_t>() {
                    return Err(Error::Error);
                }
                let status1_frame: &frames::frc_dataframe_status1_in_t =
                    unsafe { &*(frame_data.as_ptr() as *const frames::frc_dataframe_status1_in_t) };
                Ok(low_level::PeriodicStatus1 {
                    sensor_velocity: status1_frame.sensorVel
                        * if self.inverted { -1.0 } else { 1.0 },
                    motor_temperature: status1_frame.mtrTemp,
                    bus_voltage: status1_frame.mtrVoltage() as f64 / 32.0,
                    output_current: status1_frame.mtrCurrent() as f64 / 128.0,
                })
            }
        }
    }

    pub(crate) fn periodic_status_2(&self) -> Result<low_level::PeriodicStatus2> {
        match self.can.read_periodic_packet(
            frames::CMD_API_STAT2,
            self.status_period_ms[2] as i32 * 2 + self.can_timeout_ms,
            self.status_period_ms[2] as i32,
        )? {
            None => Err(Error::Timeout),
            Some(frame) => {
                let frame_data = frame.data();
                if frame_data.len() < mem::size_of::<frames::frc_dataframe_status2_in_t>() {
                    return Err(Error::Error);
                }
                let status2_frame: &frames::frc_dataframe_status2_in_t =
                    unsafe { &*(frame_data.as_ptr() as *const frames::frc_dataframe_status2_in_t) };
                let invert = if self.inverted { -1.0 } else { 1.0 };
                Ok(low_level::PeriodicStatus2 {
                    sensor_position: status2_frame.sensorPos * invert,
                    iaccum: status2_frame.iAccum * invert,
                })
            }
        }
    }

    pub(crate) fn setpoint_command(
        &mut self,
        value: f32,
        ctrl: ControlType,
        pid_slot: u8,
        arb_feedforward: f64,
        arb_ff_units: u8,
    ) {
        let api_id = ctrl as i32;
        let shifted_arb_ff = 1024.0 * arb_feedforward;
        let packed_ff = if shifted_arb_ff > 32767.0 {
            32767
        } else if shifted_arb_ff < -32767.0 {
            -32767
        } else {
            shifted_arb_ff as i16
        };
        unsafe {
            driver::set::set5(
                self.device_id as _,
                safe_float(if self.inverted { -value } else { value }),
                api_id,
                pid_slot,
                if self.inverted { -packed_ff } else { packed_ff },
                arb_ff_units,
            )
        }
    }

    pub(crate) fn set_parameter_bool(
        &mut self,
        parameter_id: ConfigParameter,
        value: bool,
    ) -> Result<()> {
        self.set_parameter_core(parameter_id, low_level::ParameterType::Bool, value as u32)
    }

    pub(crate) fn set_parameter_float(
        &mut self,
        parameter_id: ConfigParameter,
        value: f32,
    ) -> Result<()> {
        self.set_parameter_core(
            parameter_id,
            low_level::ParameterType::Float32,
            value.to_bits(),
        )
    }

    fn set_parameter_core(
        &mut self,
        parameter_id: ConfigParameter,
        type_: low_level::ParameterType,
        value: u32,
    ) -> Result<()> {
        let frame = frames::frc_dataframe_t {
            setParam_out: frames::frc_dataframe_setParam_out_t {
                parameter: value,
                parameterType: type_ as u8,
            },
        };
        self.can.write_packet(
            unsafe { &frame.data[..5] },
            frames::CMD_API_PARAM_ACCESS | parameter_id as i32,
        )?;
        Ok(())
    }

    fn parameter_core(
        &self,
        parameter_id: low_level::ConfigParameter,
        _expected_type: low_level::ParameterType,
    ) -> Result<u32> {
        // Request parameter
        let api_id = frames::CMD_API_PARAM_ACCESS | parameter_id as i32;
        self.can.write_packet(&[], api_id)?;

        // Wait for reply containing parameter
        match self.can.read_packet_timeout(api_id, self.can_timeout_ms)? {
            None => Err(Error::Timeout),
            Some(data) => {
                // Extract parameter from payload
                let payload = data.data();
                if payload.len() < mem::size_of::<frames::frc_dataframe_getParam_in_t>() {
                    Err(Error::Error)
                } else {
                    Ok(
                        unsafe {
                            *(payload.as_ptr() as *const frames::frc_dataframe_getParam_in_t)
                        }
                        .parameter0,
                    )
                }
            }
        }
    }
}

impl SparkMaxLowLevel for SparkMax {
    fn firmware_version(&self) -> (u32, bool) {
        unimplemented!()
    }

    fn set_motor_type(&mut self, type_: MotorType) -> Result<()> {
        self.set_parameter(Parameter::MotorType(type_ as u32))
    }

    fn motor_type(&self) -> Result<MotorType> {
        let value = self.parameter_u32(ConfigParameter::MotorType)?;
        if value == MotorType::Brushed as u32 {
            Ok(MotorType::Brushed)
        } else if value == MotorType::Brushless as u32 {
            Ok(MotorType::Brushless)
        } else {
            Err(Error::Error)
        }
    }

    fn set_periodic_frame_period<F: PeriodicFrame>(&mut self, period_ms: u16) -> Result<()> {
        let index = F::FRAME_ID - frames::CMD_API_STAT0;
        let frame = frames::frc_dataframe_t {
            statusConfig_out: frames::frc_dataframe_statusConfig_out_t {
                updateRate: period_ms,
                ..Default::default()
            },
        };
        self.can
            .write_packet(unsafe { &frame.data[..2] }, F::FRAME_ID)?;
        self.status_period_ms[index as usize] = period_ms;
        Ok(())
    }

    fn set_control_frame_period(&mut self, period_ms: i32) {
        unsafe { driver::set::set_ctrl_frame_period_ms(self.device_id as u32, period_ms) }
    }

    fn set_parameter(&mut self, parameter: Parameter) -> Result<()> {
        let parameter_id = parameter.id();
        self.set_parameter_core(parameter_id, parameter_id.type_(), parameter.value())
    }

    fn parameter_bool(&self, parameter_id: ConfigParameter) -> Result<bool> {
        Ok(self.parameter_core(parameter_id, low_level::ParameterType::Bool)? != 0)
    }

    fn parameter_float(&self, parameter_id: ConfigParameter) -> Result<f32> {
        Ok(f32::from_bits(self.parameter_core(
            parameter_id,
            low_level::ParameterType::Float32,
        )?))
    }

    fn parameter_u32(&self, parameter_id: ConfigParameter) -> Result<u32> {
        self.parameter_core(parameter_id, low_level::ParameterType::Uint32)
    }

    fn parameter_i32(&self, parameter_id: ConfigParameter) -> Result<i32> {
        Ok(self.parameter_core(parameter_id, low_level::ParameterType::Int32)? as i32)
    }

    fn set_enc_position(&mut self, value: f32) -> Result<()> {
        let value = if self.inverted { -value } else { value };
        unimplemented!()
    }

    fn set_iaccum(&mut self, value: f32) -> Result<()> {
        let value = if self.inverted { -value } else { value };
        unimplemented!()
    }

    fn restore_factory_defaults(&mut self, persist: bool) -> Result<()> {
        let frame = frames::frc_dataframe_t {
            setParam_out: frames::frc_dataframe_setParam_out_t {
                parameter: persist as u32,
                parameterType: low_level::ParameterType::Bool as u8,
            },
        };
        self.can
            .write_packet(unsafe { &frame.data[..5] }, frames::CMD_API_FACTORY_RESET)?;
        Ok(())
    }
}

fn safe_float(f: f32) -> f32 {
    if f.is_infinite() || f.is_nan() {
        0.0
    } else {
        f
    }
}
