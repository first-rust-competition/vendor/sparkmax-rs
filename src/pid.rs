use super::{
    low_level::{ConfigParameter, SparkMaxLowLevel},
    ControlType, Result, SparkMax,
};

#[derive(Debug, Copy, Clone, PartialEq)]
/// Arbitrary feedforward.
pub enum ArbFF {
    Voltage(f64),
    PercentOut(f64),
}

impl ArbFF {
    fn units(self) -> u8 {
        match self {
            ArbFF::Voltage(_) => 0,
            ArbFF::PercentOut(_) => 1,
        }
    }

    fn value(self) -> f64 {
        match self {
            ArbFF::Voltage(v) => v,
            ArbFF::PercentOut(v) => v,
        }
    }
}

impl Default for ArbFF {
    /// Returns a default feedforward of 0.
    #[inline]
    fn default() -> ArbFF {
        ArbFF::Voltage(0.0)
    }
}

#[repr(u8)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Slot {
    /// Slot 0
    A = 0,
    /// Slot 1
    B = 1,
    /// Slot 2
    C = 2,
    /// Slot 3
    D = 3,
}

impl Default for Slot {
    /// Returns slot A (0).
    #[inline]
    fn default() -> Slot {
        Slot::A
    }
}

fn set_gain_raw(
    device: &mut SparkMax,
    param_slot_0: ConfigParameter,
    slot: Slot,
    value: f32,
) -> Result<()> {
    let param_slot_0 = param_slot_0 as i32;
    debug_assert!(
        ConfigParameter::P_0 as i32 <= param_slot_0
            && param_slot_0 <= ConfigParameter::OutputMax_0 as i32,
        "non-PID param_slot_0 passed to set_gain_raw"
    );
    let offset = slot as i32 * 8 + param_slot_0;
    let param: ConfigParameter = unsafe { ::std::mem::transmute(offset) };
    device.set_parameter_float(param, value)
}

/// This trait is sealed and cannot be implemented outside of this crate.
pub trait PidController: private::Sealed {
    fn set_reference(
        &mut self,
        value: f32,
        ctrl: ControlType,
        pid_slot: Slot,
        arb_feedforward: ArbFF,
    );

    fn set_p(&mut self, gain: f32, slot: Slot) -> Result<()>;
    fn set_i(&mut self, gain: f32, slot: Slot) -> Result<()>;
    fn set_d(&mut self, gain: f32, slot: Slot) -> Result<()>;
    fn set_d_filter(&mut self, gain: f32, slot: Slot) -> Result<()>;
    fn set_ff(&mut self, gain: f32, slot: Slot) -> Result<()>;
    fn set_izone(&mut self, gain: f32, slot: Slot) -> Result<()>;
    fn set_output_range(&mut self, min: f32, max: f32, slot: Slot) -> Result<()>;

    // TODO

    fn set_iaccum(&mut self, iaccum: f32) -> Result<()>;
    fn iaccum(&self) -> Result<f32>;
}

impl PidController for SparkMax {
    fn set_reference(
        &mut self,
        value: f32,
        ctrl: ControlType,
        pid_slot: Slot,
        arb_feedforward: ArbFF,
    ) {
        self.setpoint_command(
            value,
            ctrl,
            pid_slot as u8,
            arb_feedforward.value(),
            arb_feedforward.units(),
        )
    }

    fn set_p(&mut self, gain: f32, slot: Slot) -> Result<()> {
        set_gain_raw(self, ConfigParameter::P_0, slot, gain)
    }
    fn set_i(&mut self, gain: f32, slot: Slot) -> Result<()> {
        set_gain_raw(self, ConfigParameter::I_0, slot, gain)
    }
    fn set_d(&mut self, gain: f32, slot: Slot) -> Result<()> {
        set_gain_raw(self, ConfigParameter::D_0, slot, gain)
    }
    fn set_d_filter(&mut self, gain: f32, slot: Slot) -> Result<()> {
        debug_assert!(gain >= 0.0, "gain cannot be negative");
        debug_assert!(gain <= 1.0, "gain must be within [0,1]");
        let gain = gain.max(0.0).min(1.0);
        set_gain_raw(self, ConfigParameter::DFilter_0, slot, gain)
    }
    fn set_ff(&mut self, gain: f32, slot: Slot) -> Result<()> {
        set_gain_raw(self, ConfigParameter::F_0, slot, gain)
    }
    fn set_izone(&mut self, gain: f32, slot: Slot) -> Result<()> {
        set_gain_raw(self, ConfigParameter::IZone_0, slot, gain)
    }
    fn set_output_range(&mut self, min: f32, max: f32, slot: Slot) -> Result<()> {
        let (min, max) = if self.inverted() {
            (-max, -min)
        } else {
            (min, max)
        };
        set_gain_raw(self, ConfigParameter::OutputMin_0, slot, min)?;
        set_gain_raw(self, ConfigParameter::OutputMax_0, slot, max)
    }

    #[inline]
    fn set_iaccum(&mut self, iaccum: f32) -> Result<()> {
        SparkMaxLowLevel::set_iaccum(self, iaccum)
    }
    fn iaccum(&self) -> Result<f32> {
        Ok(self.periodic_status_2()?.iaccum)
    }
}

mod private {
    use crate::SparkMax;

    pub trait Sealed {}
    impl Sealed for SparkMax {}
}
