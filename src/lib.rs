mod digital_in;
mod encoder;
mod error;
pub mod frames;
pub mod low_level;
pub mod pid;
mod spark_max;

pub use self::{digital_in::*, encoder::*, error::Error, low_level::MotorType, spark_max::*};

#[repr(i32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum ControlType {
    DutyCycle = frames::CMD_API_DC_SET,
    Velocity = frames::CMD_API_SPD_SET,
    Voltage = frames::CMD_API_VOLT_SET,
    Position = frames::CMD_API_POS_SET,
    SmartMotion = frames::CMD_API_SMARTMOTION_SET,
    Current = frames::CMD_API_CURRENT_SET,
    SmartVelocity = frames::CMD_API_SMART_VEL_SET,
}

pub type Result<T, E = Error> = std::result::Result<T, E>;
