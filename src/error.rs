use std::{error, fmt};
use wpilib::HalError;

#[derive(Debug, Copy, Clone)]
pub enum Error {
    Error,
    Timeout,
    HalError(HalError),
    FollowConfigMismatch,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::HalError(e) => e.fmt(f),
            Error::Timeout => f.write_str("A timeout occurred communicating with a SPARK MAX."),
            Error::Error => f.write_str("An unspecified SPARK MAX error occurred."),
            Error::FollowConfigMismatch => f.write_str("Follower config setup check failed"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Error::HalError(e) => Some(e),
            _ => None,
        }
    }
}

impl From<HalError> for Error {
    fn from(e: HalError) -> Error {
        Error::HalError(e)
    }
}
