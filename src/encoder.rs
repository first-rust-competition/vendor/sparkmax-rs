use super::{
    low_level::{ConfigParameter, Parameter, SparkMaxLowLevel},
    Result, SparkMax,
};

/// Interface for getting position and velocity, and configuring conversion factors.
///
/// This trait is sealed and cannot be implemented outside of this crate.
pub trait Encoder: private::Sealed {
    fn position(&self) -> Result<f32>;
    fn velocity(&self) -> Result<f32>;
    fn set_position(&mut self, position: f32) -> Result<()>;
    fn set_position_conversion_factor(&mut self, factor: f32) -> Result<()>;
    fn set_velocity_conversion_factor(&mut self, factor: f32) -> Result<()>;
    fn position_conversion_factor(&self) -> Result<f32>;
    fn velocity_conversion_factor(&self) -> Result<f32>;
}

impl Encoder for SparkMax {
    fn position(&self) -> Result<f32> {
        Ok(self.periodic_status_2()?.sensor_position)
    }

    fn velocity(&self) -> Result<f32> {
        Ok(self.periodic_status_1()?.sensor_velocity)
    }

    fn set_position(&mut self, position: f32) -> Result<()> {
        self.set_enc_position(position)
    }

    fn set_position_conversion_factor(&mut self, factor: f32) -> Result<()> {
        self.set_parameter(Parameter::PositionConversionFactor(factor))
    }

    fn set_velocity_conversion_factor(&mut self, factor: f32) -> Result<()> {
        self.set_parameter(Parameter::VelocityConversionFactor(factor))
    }

    fn position_conversion_factor(&self) -> Result<f32> {
        self.parameter_float(ConfigParameter::PositionConversionFactor)
    }

    fn velocity_conversion_factor(&self) -> Result<f32> {
        self.parameter_float(ConfigParameter::VelocityConversionFactor)
    }
}

mod private {
    use super::SparkMax;

    pub trait Sealed {}
    impl Sealed for SparkMax {}
}
