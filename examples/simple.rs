use sparkmax::{self, SparkMax};
use wpilib;

#[derive(Debug)]
struct Robot {
    motor: SparkMax,
}

impl wpilib::IterativeRobot for Robot {
    fn teleop_periodic(&mut self) {
        self.motor.set(0.5);
    }
}

fn main() -> sparkmax::Result<()> {
    let base = wpilib::RobotBase::new().expect("Error initializing WPILib");
    let ds = base.make_ds();

    let mut robot = Robot {
        motor: SparkMax::new(1, sparkmax::MotorType::Brushless)?,
    };

    wpilib::start_timed(&mut robot, &ds);
    Ok(())
}
