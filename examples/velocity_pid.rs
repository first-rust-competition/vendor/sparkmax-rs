// In order to use PID functionality, we must use the PidController trait.
// We also use the Encoder trait to get velocity values.
// The SparkMaxLowLevel trait is required for the restore_factory_defaults method.
use sparkmax::{
    self,
    low_level::SparkMaxLowLevel as _,
    pid::{self, PidController as _},
    Encoder as _, SparkMax,
};
use std::time;
use wpilib;

const MAX_RPM: f32 = 5700.0;

#[derive(Debug)]
struct Robot<'a> {
    ds: &'a wpilib::ds::DriverStation<'a>,
    stick_port: wpilib::ds::JoystickPort,
    stick_axis: wpilib::ds::JoystickAxis,
    motor: SparkMax,
    last_print_time: time::Instant,
}

impl wpilib::IterativeRobot for Robot<'_> {
    fn teleop_periodic(&mut self) {
        // read setpoint from joystick and scale by max rpm
        let setpoint = MAX_RPM
            * self
                .ds
                .stick_axis(self.stick_port, self.stick_axis)
                .unwrap_or(0.0);

        /*
         * The SPARK MAX PID controller can be commanded a set point using the
         * set_reference method from the sparkmax::pid::PidController trait.
         *
         * The first parameter is the value of the set point, whose units vary
         * depending on the control type set in the second parameter.
         *
         * The second parameter is the control type.
         */
        self.motor.set_reference(
            setpoint,
            sparkmax::ControlType::Velocity,
            pid::Slot::A,
            pid::ArbFF::Voltage(0.0),
        );

        // Print every 1 second.
        if self.last_print_time.elapsed() >= time::Duration::from_secs(1) {
            println!("setpoint: {}", setpoint);
            match self.motor.velocity() {
                Ok(v) => println!("process variable: {}", v),
                Err(e) => println!("error getting velocity: {:?} ({})", e, e),
            }
            self.last_print_time = time::Instant::now();
        }
    }
}

fn main() -> sparkmax::Result<()> {
    let base = wpilib::RobotBase::new().expect("Failed to initialize WPILib");
    let ds = base.make_ds();
    let mut motor = SparkMax::new(1, sparkmax::MotorType::Brushless)?;

    // The restore_factory_defaults method can be used to reset the
    // configuration parameters in the SPARK MAX to their factory default state.
    // The argument specifies whether this will persist between power cycles.
    motor.restore_factory_defaults(false)?;

    // Set PID coefficients
    motor.set_p(5e-5, pid::Slot::A)?;
    motor.set_i(1e-6, pid::Slot::A)?;
    motor.set_d(0.0, pid::Slot::A)?;
    motor.set_izone(0.0, pid::Slot::A)?;
    motor.set_ff(0.0, pid::Slot::A)?;
    motor.set_output_range(-1.0, 1.0, pid::Slot::A)?;

    let mut robot = Robot {
        ds: &ds,
        stick_port: wpilib::ds::JoystickPort::new(0).unwrap(),
        // Y axis on joystick
        stick_axis: wpilib::ds::JoystickAxis::new(1).unwrap(),
        motor,
        last_print_time: time::Instant::now(),
    };

    wpilib::start_timed(&mut robot, &ds);
    Ok(())
}
