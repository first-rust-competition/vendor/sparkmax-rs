use sparkmax::{self, SparkMax};
use std::{thread, time::Duration};

fn main() -> sparkmax::Result<()> {
    let mut motor = SparkMax::new(1, sparkmax::MotorType::Brushless)?;

    println!(
        "Forward limit enabled: {:?}",
        motor.forward_limit_switch().is_limit_switch_enabled()
    );
    println!(
        "Reverse limit enabled: {:?}",
        motor.reverse_limit_switch().is_limit_switch_enabled()
    );

    loop {
        println!(
            "Forward limit switch: {:?}",
            motor.forward_limit_switch().get()
        );
        println!(
            "Reverse limit switch: {:?}",
            motor.reverse_limit_switch().get()
        );

        thread::sleep(Duration::from_millis(50));
    }
}
